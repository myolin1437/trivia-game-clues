## Clue
| id | row_index | column_index | answer | question | game_id | board_index | value |category_id | invalid_count | canon |
|-|-|-|-|-|-|-|-|-|-|-|
| 1 | 1 | 1 | the Jordan | River mentioned most often in the Bible | 1 |  0 | 100 |       22210 | 0 | t



## Movie
| id | row_index | column_index | answer |
|-|-|-|-|
| movie_name | string | no | no |
| release_date | date | no | no |
| runtime | int | no | no |
| synopsis | text | no | no |
| picture_url | string | no | no |
from urllib import response
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg

from .categories import CategoryOut

router = APIRouter()

class ClueIn(BaseModel):
    answer: str
    question: str
    value: int
    invalid_count: int
    category_id: int
    canon: bool


class ClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool


class Clues(BaseModel):
    page_count: int
    clues: list[ClueOut]


class Message(BaseModel):
    message: str


select_clue_query = """
    SELECT clues.id AS clue_id, clues.answer, 
        clues.question, clues.value,
        clues.invalid_count, clues.canon AS clue_canon,
        categories.id AS category_id, categories.title,
        categories.canon AS category_canon
    FROM categories
    JOIN clues 
    ON categories.id = clues.category_id
"""

clue_fields = [
    "clue_id",
    "answer",
    "question",
    "value",
    "invalid_count",
    "clue_canon",
]

def build_clue_object(row, description):
    record = {}
    category = {}

    for i, column in enumerate(description):
        if column.name in clue_fields:
            record[column.name] = row[i]
        else: 
            category[column.name] = row[i]

    category["id"] = category["category_id"]
    category["canon"] = category["category_canon"]

    record["id"] = record["clue_id"]
    record["canon"] = record["clue_canon"]

    record["category"] = category

    return record


@router.get(
    "/api/clues/{clue_id}",
    response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                {select_clue_query}
                WHERE clues.id = %s
                """,
                [clue_id],
            )

            record = None
            row = cur.fetchone()
            description = cur.description
            if row:
                record = build_clue_object(row, description)

            if record is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                # return {"message": "Clue not found"}

            return record


@router.get(
    "/api/random-clue",
    response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_random_clue(response: Response, valid: bool=True):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            filter_if_valid = "WHERE clues.invalid_count = 0"
            if valid:
                filter_if_valid = "WHERE clues.invalid_count = 0"
            else:
                filter_if_valid = ""
            
            cur.execute(
                f"""
                {select_clue_query}
                {filter_if_valid}
                ORDER BY RANDOM() LIMIT 1
            """
            )

            row = cur.fetchone()
            description = cur.description
            return build_clue_object(row, description)


@router.get("/api/clues", response_model=Clues)
def clues_list(value: int = None, page: int = 0):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            if value:
                filter_if_value = f"WHERE clues.value = {value}"
            else:
                filter_if_value = ""

            cur.execute(
                f"""
                {select_clue_query}
                {filter_if_value}
                ORDER BY clues.id
                LIMIT 100 OFFSET %s
            """, 
                    [page*100],
            )

            results = []
            description = cur.description
            for row in cur.fetchall():
                results.append(build_clue_object(row, description))

            cur.execute(
                """
                SELECT COUNT(*) FROM clues;
            """
            )
            raw_count = cur.fetchone()[0]
            page_count = (raw_count // 100) + 1

            return Clues(page_count=page_count, clues=results)


@router.delete(
    "/api/clues/{clue_id}", 
    response_model=ClueOut,
    responses={404: {"model": Message}}
)
def delete_clue(clue_id: int, response:Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                UPDATE clues
                SET invalid_count = invalid_count + 1
                WHERE id = %s;
            """,
                    [clue_id],
            )

            cur.execute(
                f"""
                {select_clue_query}
                WHERE clues.id = %s
            """, 
                    [clue_id],
            )
    
            record = None
            row = cur.fetchone()
            description = cur.description
            if row:
                record = build_clue_object(row, description)

            if record is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                # return {"message": "Clue not found"}

            return record
from datetime import datetime
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg

router = APIRouter()


class GameOut(BaseModel):
    id: int
    episode_id: int
    aired: str
    canon: bool
    total_amount_won: int


class Category(BaseModel):
    id: int
    title: str


class Clue(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: Category


class CustomGame(BaseModel):
    id: int
    created_on: datetime
    clues: list[Clue]


class Message(BaseModel):
    message: str


@router.get(
    "/api/games/{game_id}",
    response_model=GameOut,
    responses={404: {"model": Message}},
)
def get_gane(game_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT games.id AS game_id, games.episode_id, 
                    games.aired, games.canon,
                    SUM(clues.value) AS total_amount_won
                FROM games
                LEFT OUTER JOIN clues
                ON games.id = clues.game_id
                WHERE games.id = %s
                GROUP BY games.id, games.episode_id, 
                    games.aired, games.canon
                """,
                    [game_id],
            )
        
            record = None
            row = cur.fetchone()
            if row:
                record = {}
                for i, column in enumerate(cur.description):
                    record[column.name] = row[i]

            record["id"] = record["game_id"]

            if record is None:
                response.status_code = status.HTTP_404_NOT_FOUND

            return record


@router.post(
    "/api/custom-games",
    response_model=CustomGame,
    responses={409: {"model": Message}},
)
def create_game(response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT clues.id AS clue_id, clues.answer, 
                    clues.question, clues.value, clues.invalid_count,
                    categories.id AS category_id, categories.title
                FROM categories
                LEFT JOIN clues 
                ON clues.category_id = categories.id
                WHERE clues.canon = true
                ORDER BY RANDOM() LIMIT 30
                """
            )

            clue_fields = [
                "clue_id",
                "answer",
                "question",
                "value",
                "invalid_count",
            ]

            clues_list = []
            for row in cur.fetchall():
                clue = {}
                category = {}

                for i, column in enumerate(cur.description):
                    if column.name in clue_fields:
                        clue[column.name] = row[i]
                    else: 
                        category[column.name] = row[i]

                clue["id"] = clue["clue_id"]
                category["id"] = category["category_id"]

                clue["category"] = category

                clues_list.append(clue)

            with conn.transaction():
                cur.execute(
                    """
                    INSERT INTO game_definitions (created_on)
                    VALUES (CURRENT_TIMESTAMP)
                    RETURNING *
                    """
                )

                custom_game = cur.fetchone()
                custom_game_id = custom_game[0]
                custom_game_timestamp = custom_game[1]

                for clue in clues_list:
                    cur.execute(
                        """
                        INSERT INTO game_definition_clues (game_definition_id, clue_id)
                        VALUES (%s, %s)
                        """,
                            [custom_game_id, clue["id"]]
                    )
                
                record = {
                    "id": custom_game_id,
                    "created_on": custom_game_timestamp,
                    "clues": clues_list
                }

                return record


@router.get(
    "/api/custom-games/{custom_game_id}",
    response_model=CustomGame,
    responses={404: {"model": Message}},
)
def get_custom_game(custom_game_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            # cur. execute(
            #     """
            #     SELECT clues.id AS clue_id, clues.answer, 
            #         clues.question, clues.value, clues.invalid_count,
            #         categories.id AS category_id, categories.title, 
            #         gd.id AS game_def_id, gd.created_on 
            #         FROM categories 
            #         RIGHT JOIN clues ON categories.id = clues.category_id
            #         RIGHT JOIN game_definition_clues AS gdc ON clues.id = gdc.clue_id
            #         RIGHT JOIN game_definitions AS gd ON gd.id = gdc.game_definition_id
            #         WHERE gd.id = %s
            #     """,
            #         [custom_game_id],
            # )
            cur.execute(
                """
                SELECT
                    gd.created_on, 
                    clues.id AS clue_id, clues.answer, 
                    clues.question, clues.value, clues.invalid_count,
                    categories.id AS category_id, categories.title
                FROM game_definitions AS gd
                LEFT JOIN game_definition_clues AS gdc ON gd.id = gdc.game_definition_id
                LEFT JOIN clues ON clues.id = gdc.clue_id
                LEFT JOIN categories ON categories.id = clues.category_id
                WHERE gd.id = %s
            """,
            [custom_game_id],
            )

            rows = cur.fetchall()
            timestamp = rows[0][0]
            
            clue_fields = [
                "clue_id",
                "answer",
                "question",
                "value",
                "invalid_count",
            ]

            clues_list = []
            for row in rows:
                clue = {}
                category = {}
                for i, column in enumerate(cur.description):
                    if column.name in clue_fields:
                        clue[column.name] = row[i]
                    else: 
                        category[column.name] = row[i]

                clue["id"] = clue["clue_id"]
                category["id"] = category["category_id"]

                clue["category"] = category

                clues_list.append(clue) 

            record = {
                "id": custom_game_id,
                "created_on": timestamp,
                "clues": clues_list
            }
            return record

            
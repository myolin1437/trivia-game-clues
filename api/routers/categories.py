# Line 1-10 are setup for using pymongo
import os
import bson
import pymongo

dbhost = os.environ['MONGOHOST']
dbname = os.environ['MONGODATABASE']
dbuser = os.environ['MONGOUSER']
dbpass = os.environ['MONGOPASSWORD']
mongo_str = f"mongodb://{dbuser}:{dbpass}@{dbhost}"

from typing import Union
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg

# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()


class CategoryIn(BaseModel):
    title: str


class CategoryOut(BaseModel):
    id: Union[int, str]
    title: str
    canon: bool


class CategoryWithClueCount(CategoryOut):
    num_clues: int


class Categories(BaseModel):
    page_count: int
    categories: list[CategoryWithClueCount]


class Message(BaseModel):
    message: str

# PART OF REFACTORED CODES USING PYMONGO
client = pymongo.MongoClient(mongo_str)
db = client[dbname]

@router.get("/api/categories", response_model=Categories)
# ORIGINAL CODES USING PSYCOPG
# def categories_list(page: int = 0):
#     # Uses the environment variables to connect
#     # In development, see the docker-compose.yml file for
#     #   the PG settings in the "environment" section
#     with psycopg.connect() as conn:
#         with conn.cursor() as cur:
#             cur.execute(
#                 """
#                 SELECT categories.id, categories.title, categories.canon,
#                     COUNT (clues.*) AS num_clues
#                 FROM categories
#                 LEFT OUTER JOIN clues
#                 ON categories.id = clues.category_id
#                 GROUP BY categories.id, categories.title, categories.canon
#                 ORDER BY title
#                 LIMIT 100 OFFSET %s
#             """,
#                 [page * 100],
#             )

#             results = []
#             for row in cur.fetchall():
#                 record = {}
#                 for i, column in enumerate(cur.description):
#                     record[column.name] = row[i]
#                 results.append(record)

#             cur.execute(
#                 """
#                 SELECT COUNT(*) FROM categories;
#             """
#             )
#             raw_count = cur.fetchone()[0]
#             page_count = (raw_count // 100) + 1

#             return Categories(page_count=page_count, categories=results)

# REFACTORED CODES USING PYMONGO
def categories_list(page: int = 0):
    categories = db.categories.find().sort("title").skip(100*page).limit(100)
    categories = list(categories)
    for category in categories:
        count = db.command({
            "count": "clues",
            "query": { "category_id": category["_id"] }
        })
        category["num_clues"] = count["n"]
        category["id"] = category["_id"]
        del category["_id"]
    page_count = db.command({"count": "categories"})["n"] // 100
    return {
        "page_count": page_count,
        "categories": categories,
    }


@router.get(
    "/api/categories/{category_id}",
    response_model=CategoryOut,
    responses={404: {"model": Message}},
)
# ORIGINAL CODES USING PSYCOPG
# def get_category(category_id: int, response: Response):
#     with psycopg.connect() as conn:
#         with conn.cursor() as cur:
#             cur.execute(
#                 """
#                 SELECT id, title, canon
#                 FROM categories
#                 WHERE id = %s
#             """,
#                 [category_id],
#             )
#             row = cur.fetchone()
#             if row is None:
#                 response.status_code = status.HTTP_404_NOT_FOUND
#                 return {"message": "Category not found"}
#             record = {}
#             for i, column in enumerate(cur.description):
#                 record[column.name] = row[i]
#             return record

# REFACTORED CODES USING PYMONGO
def get_category(category_id: int, response: Response):
    result = db.categories.find_one({"_id": category_id})
    if result is None:
        response.status_code = status.HTTP_404_NOT_FOUND
    result["id"] = result["_id"]
    del result["_id"] # still works without deleting
    return result


@router.post(
    "/api/categories",
    response_model=CategoryOut,
    responses={409: {"model": Message}},
)
# ORIGINAL CODES USING PSYCOPG
# def create_category(category: CategoryIn, response: Response):
#     with psycopg.connect() as conn:
#         with conn.cursor() as cur:
#             try:
#                 # Uses the RETURNING clause to get the data
#                 # just inserted into the database. See
#                 # https://www.postgresql.org/docs/current/sql-insert.html
#                 cur.execute(
#                     """
#                     INSERT INTO categories (title, canon)
#                     VALUES (%s, false)
#                     RETURNING id, title, canon;
#                 """,
#                     [category.title],
#                 )
#             except psycopg.errors.UniqueViolation:
#                 # status values at https://github.com/encode/starlette/blob/master/starlette/status.py
#                 response.status_code = status.HTTP_409_CONFLICT
#                 return {
#                     "message": "Could not create duplicate category",
#                 }
#             row = cur.fetchone()
#             record = {}
#             for i, column in enumerate(cur.description):
#                 record[column.name] = row[i]
#             return record

# REFACTORED CODES USING PYMONGO
def create_category(category: CategoryIn, response: Response):
    db.categories.insert_one({"title": category.title, "canon": False})
    result = db.categories.find_one({"title": category.title})
    if isinstance(result["_id"], bson.objectid.ObjectId):
        result["_id"] = str(result["_id"])
    result["id"] = result["_id"]
    del result["_id"]
    return result

    # A WAY TO ALWAYS HAVE ID AS INT
    # last_item = db.categories.find_one(
    #     {},
    #     { "_id": 1, "total": 1 },
    #     sort=[("_id", pymongo.DESCENDING)]
    # )
    # last_id = last_item["_id"]
    # record = category.dict()
    # record["_id"] = last_id + 1
    # record["canon"] = False
    # db.categories.insert_one(record)
    # record["id"] = record["_id"]
    # del record["_id"]
    # return record


@router.put(
    "/api/categories/{category_id}",
    response_model=CategoryOut,
    responses={404: {"model": Message}},
)
# ORIGINAL CODES USING PSYCOPG
# def update_category(category_id: int, category: CategoryIn, response: Response):
#     with psycopg.connect() as conn:
#         with conn.cursor() as cur:
#             cur.execute(
#                 """
#                 UPDATE categories
#                 SET title = %s
#                 WHERE id = %s;
#             """,
#                 [category.title, category_id],
#             )
#     return get_category(category_id, response)

# REFACTORED CODES USING PYMONGO
def update_category(category_id: Union[int, str], category: CategoryIn, response: Response):
    result = db.categories.find_one_and_update(
        {"_id": category_id},
        {"$set": {
            "title": category.title, 
            "canon": False
        }}
    )
    return result
    # db.categories.update_one(
    #     {"_id": category_id},
    #     {"$set": {
    #         "title": category.title, 
    #         "canon": False
    #     }}
    # )
    # return get_category(category_id, response)


@router.delete(
    "/api/categories/{category_id}",
    response_model=Message,
    responses={400: {"model": Message}},
)
def remove_category(category_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(
                    """
                    DELETE FROM categories
                    WHERE id = %s;
                """,
                    [category_id],
                )
                return {
                    "message": "Success",
                }
            except psycopg.errors.ForeignKeyViolation:
                response.status_code = status.HTTP_400_BAD_REQUEST
                return {
                    "message": "Cannot delete category because it has clues",
                }
